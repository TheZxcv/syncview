using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;

namespace SyncView.Api.Services
{
    public class KeyValueStore : IKeyValueStore
    {
        private readonly IDistributedCache _distributedCache;

        public KeyValueStore(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public async Task<T?> GetAsync<T>(string key) where T : class
        {
            string? json = await _distributedCache.GetStringAsync(key);
            if (json != null)
            {
                return JsonSerializer.Deserialize<T>(json);
            }

            return null;
        }

        public Task PutAsync<T>(string key, T value) where T : class
        {
            string json = JsonSerializer.Serialize(value);
            return _distributedCache.SetStringAsync(key, json, new DistributedCacheEntryOptions()
            {
                SlidingExpiration = TimeSpan.FromHours(1),
            });
        }
    }
}
