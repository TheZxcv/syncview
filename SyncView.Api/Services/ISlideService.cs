using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SyncView.Api.Models;

namespace SyncView.Api.Services
{
    public interface ISlideService
    {
        Task<IReadOnlyList<SlideMetadata>> GetAllAsync();
        Task<Slide?> GetAsync(Guid uuid);
        Task<SignedUploadUrl> GetUploadUrlAsync(Guid uuid, FileMetadata metadata);
        Task<Slide> CreateAsync(SlideMetadata slide);
        Task<Slide> UpdateAsync(Slide slide);
    }
}
