using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using SyncView.Api.Entities;
using SyncView.Api.Models;
using SyncView.Api.Repositories;
using SyncView.Api.Utils;

namespace SyncView.Api.Services
{
    public class SlideService : ISlideService
    {
        private readonly ILogger<ISlideService> _logger;
        private readonly ISlideRepository _slideRepository;
        private readonly IWebStorageService _storageService;
        private readonly IMapper _mapper;

        public SlideService(ILogger<ISlideService> logger,
            ISlideRepository slideRepository,
            IWebStorageService storageService,
            IMapper mapper)
        {
            _logger = Ensure.NotNull(logger, nameof(logger));
            _slideRepository = Ensure.NotNull(slideRepository, nameof(slideRepository));
            _storageService = Ensure.NotNull(storageService, nameof(storageService));
            _mapper = Ensure.NotNull(mapper, nameof(mapper));
        }

        public async Task<Slide?> GetAsync(Guid uuid)
        {
            var item = await _slideRepository.GetAsync(uuid);

            if (item == null)
            {
                return null;
            }

            var result = _mapper.Map<Slide>(item);
            result.Url = await _storageService.GetUrlAsync(item.Uuid);

            return result;
        }

        public Task<SignedUploadUrl> GetUploadUrlAsync(Guid uuid, FileMetadata metadata)
        {
            return _storageService.PrepareUploadAsync(uuid, metadata);
        }

        public async Task<Slide> CreateAsync(SlideMetadata slide)
        {
            var entity = _mapper.Map<SlideEntity>(slide);
            var written = await _slideRepository.AddAsync(entity);
            return _mapper.Map<Slide>(written);
        }

        public Task<Slide> UpdateAsync(Slide slide)
        {
            throw new NotImplementedException();
        }

        public async Task<IReadOnlyList<SlideMetadata>> GetAllAsync()
        {
            return (await _slideRepository.GetAllAsync())
                .Select(item => _mapper.Map<SlideMetadata>(item))
                .ToList();
        }
    }
}
