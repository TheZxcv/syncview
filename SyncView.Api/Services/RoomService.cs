using System;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.Extensions.Logging;
using SyncView.Api.Models;
using SyncView.Api.Repositories;
using SyncView.Api.Utils;

namespace SyncView.Api.Services
{
    public class RoomService : IRoomService
    {
        private readonly ILogger<IRoomService> _logger;
        private readonly IRoomRepository _roomRepository;
        private readonly ISlideService _slideService;
        private readonly IKeyValueStore _keyValueStore;
        private readonly IMapper _mapper;

        public RoomService(ILogger<IRoomService> logger,
            IRoomRepository roomRepository,
            ISlideService slideService,
            IKeyValueStore keyValueStore,
            IMapper mapper)
        {
            _logger = Ensure.NotNull(logger, nameof(logger));
            _roomRepository = Ensure.NotNull(roomRepository, nameof(roomRepository));
            _slideService = Ensure.NotNull(slideService, nameof(slideService));
            _keyValueStore = Ensure.NotNull(keyValueStore, nameof(keyValueStore));
            _mapper = Ensure.NotNull(mapper, nameof(mapper));
        }

        public async Task<Room?> GetAsync(string id)
        {
            var item = await _roomRepository.GetAsync(id);

            if (item == null)
            {
                return null;
            }

            var result = _mapper.Map<Room>(item);
            result.Slide = await _slideService.GetAsync(item.slideId) ??
                           throw new InvalidOperationException("Room has null slide");

            return result;
        }

        public async Task<Room> CreateAsync(Guid slideUuid)
        {
            var room = await _roomRepository.CreateAsync(slideUuid);
            return (await GetAsync(room.Id))!;
        }

        public async Task<RoomState?> GetStateAsync(string id)
        {
            // TODO: should I check whether the room exists?
            return (await _keyValueStore.GetAsync<RoomState>(id)) ?? new RoomState(1);
        }

        public async Task<RoomState> UpdateStateAsync(string id, RoomState newState)
        {
            var state = new RoomState(newState.Page);
            await _keyValueStore.PutAsync(id, state);
            return state;
        }
    }
}
