using System.Threading.Tasks;

namespace SyncView.Api.Services
{
    public interface IKeyValueStore
    {
        public Task<T?> GetAsync<T>(string key) where T : class;
        public Task PutAsync<T>(string key, T value) where T : class;
    }
}
