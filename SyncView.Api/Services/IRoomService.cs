using System;
using System.Threading.Tasks;
using SyncView.Api.Models;

namespace SyncView.Api.Services
{
    public interface IRoomService
    {
        Task<Room?> GetAsync(string id);
        Task<Room> CreateAsync(Guid slideUuid);
        Task<RoomState?> GetStateAsync(string id);
        Task<RoomState> UpdateStateAsync(string id, RoomState newState);
    }
}
