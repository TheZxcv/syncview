using System;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Microsoft.Extensions.Logging;
using SyncView.Api.Models;
using SyncView.Api.Utils;

namespace SyncView.Api.Services
{
    public class S3StorageService : IWebStorageService
    {
        private readonly ILogger<S3StorageService> _logger;
        private readonly AwsSettings _awsSettings;
        private readonly IAmazonS3 _s3Client;

        public S3StorageService(ILogger<S3StorageService> logger, AwsSettings awsSettings)
        {
            _logger = Ensure.NotNull(logger, nameof(logger));
            _awsSettings = Ensure.NotNull(awsSettings, nameof(awsSettings));

            var bucketRegion = RegionEndpoint.GetBySystemName(_awsSettings.Region);
            _s3Client = new AmazonS3Client(_awsSettings.AccessKey, _awsSettings.SecretKey, bucketRegion);
        }

        public Task<string?> GetUrlAsync(Guid uuid)
        {
            var request = new GetPreSignedUrlRequest
            {
                BucketName = _awsSettings.BucketName,
                Key = uuid.ToString(),
                Verb = HttpVerb.GET,
                Expires = DateTime.UtcNow.AddHours(1),
            };

            var url = _s3Client.GetPreSignedURL(request);

            return Task.FromResult<string?>(url);
        }

        public Task<SignedUploadUrl> PrepareUploadAsync(Guid uuid, FileMetadata metadata)
        {
            if (metadata.FileType != "application/pdf") throw new ArgumentException("Only PDF files are allowed.");

            var request = new GetPreSignedUrlRequest
            {
                BucketName = _awsSettings.BucketName,
                Key = uuid.ToString(),
                Verb = HttpVerb.PUT,
                Expires = DateTime.UtcNow.AddMinutes(10),
                ContentType = metadata.FileType,
            };

            var signedUrl = new SignedUploadUrl(_s3Client.GetPreSignedURL(request));
            return Task.FromResult(signedUrl);
        }
    }
}
