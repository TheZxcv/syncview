using System;
using System.Threading.Tasks;
using SyncView.Api.Models;

namespace SyncView.Api.Services
{
    public interface IWebStorageService
    {
        public Task<string?> GetUrlAsync(Guid uuid);
        public Task<SignedUploadUrl> PrepareUploadAsync(Guid uuid, FileMetadata metadata);
    }
}
