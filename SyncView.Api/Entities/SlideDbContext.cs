using Microsoft.EntityFrameworkCore;

namespace SyncView.Api.Entities
{
    public class SlideDbContext : DbContext
    {
        public SlideDbContext(DbContextOptions<SlideDbContext> options)
           : base(options)
        {
        }

        // null is not null, trust me I've got a degree, lol.
        public DbSet<SlideEntity> Slides { get; set; } = null!;
        public DbSet<RoomEntity> Rooms { get; set; } = null!;
    }
}
