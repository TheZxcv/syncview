using System;
using System.ComponentModel.DataAnnotations;

namespace SyncView.Api.Entities
{
    public class SlideEntity
    {
        [Key]
        public Guid Uuid { get; set; } = Guid.Empty;
        [Required]
        public string FileName { get; set; } = string.Empty;
        [Required]
        public string Description { get; set; } = string.Empty;
    }
}
