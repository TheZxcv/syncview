using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SyncView.Api.Models;
using SyncView.Api.Services;
using SyncView.Api.Utils;

namespace SyncView.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SlideController : ControllerBase
    {
        private readonly ILogger<SlideController> _logger;
        private readonly ISlideService _slideService;

        public SlideController(ILogger<SlideController> logger, ISlideService slideService)
        {
            _logger = Ensure.NotNull(logger, nameof(logger));
            _slideService = Ensure.NotNull(slideService, nameof(slideService));
        }

        [HttpGet(Name = nameof(GetAllSlidesAsync))]
        public async Task<ActionResult<IEnumerable<Slide>>> GetAllSlidesAsync()
        {
            var items = await _slideService.GetAllAsync();
            return Ok(items);
        }

        [HttpGet]
        [Route("{uuid}", Name = nameof(GetSlideAsync))]
        public async Task<ActionResult<Slide>> GetSlideAsync(Guid uuid)
        {
            var item = await _slideService.GetAsync(uuid);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPost(Name = nameof(AddSlideAsync))]
        public async Task<ActionResult<SlideMetadata>> AddSlideAsync([FromBody] SlideMetadata item)
        {
            var created = await _slideService.CreateAsync(item);

            _logger.LogInformation($"Created slide {created.Uuid}");

            return CreatedAtRoute(nameof(GetSlideAsync), new
            {
                uuid = created.Uuid
            }, created);
        }

        [HttpPost]
        [Route("{uuid}", Name = nameof(SignForUploadAsync))]
        public async Task<ActionResult<SignedUploadUrl>> SignForUploadAsync(Guid uuid, [FromBody] FileMetadata metadata)
        {
            try
            {
                var signedUrl = await _slideService.GetUploadUrlAsync(uuid, metadata);
                return Ok(signedUrl);
            }
            catch (ArgumentException e)
            {
                _logger.LogError(e, "Bad Request");
                return BadRequest();
            }
        }
    }
}
