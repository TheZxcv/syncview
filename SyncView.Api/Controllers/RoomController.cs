using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SyncView.Api.Models;
using SyncView.Api.Services;
using SyncView.Api.Utils;

namespace SyncView.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RoomController : ControllerBase
    {
        private readonly ILogger<RoomController> _logger;
        private readonly IRoomService _roomService;

        public RoomController(ILogger<RoomController> logger, IRoomService roomService)
        {
            _logger = Ensure.NotNull(logger, nameof(logger));
            _roomService = Ensure.NotNull(roomService, nameof(roomService));
        }

        [HttpGet]
        [Route("{id}", Name = nameof(GetRoomAsync))]
        public async Task<ActionResult<Slide>> GetRoomAsync(string id)
        {
            var item = await _roomService.GetAsync(id);

            if (item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPost(Name = nameof(CreateRoomAsync))]
        public async Task<ActionResult<SlideMetadata>> CreateRoomAsync([FromBody] CreateRoom item)
        {
            var room = await _roomService.CreateAsync(item.Uuid);

            _logger.LogInformation($"Created room {room.Id}");

            return CreatedAtRoute(nameof(GetRoomAsync), new
            {
                id = room.Id,
            }, room);
        }
    }
}
