using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SyncView.Api.Models;
using SyncView.Api.Services;
using SyncView.Api.Utils;

namespace SyncView.Api.Hubs
{
    public class RoomHub : Hub
    {
        private readonly IRoomService _roomService;

        public RoomHub(IRoomService roomService)
        {
            _roomService = Ensure.NotNull(roomService, nameof(roomService));
        }

        public override async Task OnConnectedAsync()
        {
            var roomId = GetRoomId();

            await base.OnConnectedAsync();

            var room = await _roomService.GetStateAsync(roomId) ??
                       throw new InvalidOperationException("Invalid room ID");
            await Clients.Caller.SendAsync("pageChange", room);

            await Groups.AddToGroupAsync(this.Context.ConnectionId, roomId);
        }

        public async Task ChangePage(RoomState newState)
        {
            string roomId = GetRoomId();
            var updatedState = await _roomService.UpdateStateAsync(roomId, newState);
            await Clients.Group(roomId).SendAsync("pageChange", updatedState);
        }

        private string GetRoomId()
        {
            return Context.GetHttpContext().Request.RouteValues["roomId"].ToString()!;
        }
    }
}
