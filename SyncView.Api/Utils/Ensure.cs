using System;
using System.Diagnostics;

namespace SyncView.Api.Utils
{
    public static class Ensure
    {
        [DebuggerStepThrough]
        public static T NotNull<T>(T? obj, string objName) where T : class
        {
            return obj ?? throw new ArgumentNullException(objName);
        }
    }
}
