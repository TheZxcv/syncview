using System;

namespace SyncView.Api.Utils
{
    public class RandomIdGenerator
    {
        private readonly Random _random;
        private readonly int _nbytes;

        private RandomIdGenerator()
        {
            _random = new Random();
            // a base64 char is 6 bit
            // a byte is 8 bit
            // 6 bytes are 48 bits which is evenly divisible by 6.
            _nbytes = 6;
        }

        public string NextId()
        {
            var buffer = new byte[_nbytes];
            _random.NextBytes(buffer);
            return Convert.ToBase64String(buffer);
        }

        public static RandomIdGenerator Instance = new RandomIdGenerator();
    }
}
