using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SyncView.Api.Entities;
using SyncView.Api.Utils;

namespace SyncView.Api.Repositories
{
    public class SlideRepository : ISlideRepository
    {
        private readonly SlideDbContext _slideDbContext;

        public SlideRepository(SlideDbContext slideDbContext)
        {
            _slideDbContext = Ensure.NotNull(slideDbContext, nameof(slideDbContext));
        }

        public async Task<SlideEntity?> GetAsync(Guid uuid)
        {
            return await _slideDbContext.Slides.FindAsync(uuid);
        }

        public async Task<SlideEntity> AddAsync(SlideEntity item)
        {
            await _slideDbContext.Slides.AddAsync(item);
            await _slideDbContext.SaveChangesAsync();

            return item;
        }

        public async Task<SlideEntity?> DeleteAsync(Guid uuid)
        {
            var item = await GetAsync(uuid);
            if (item != null)
            {
                _slideDbContext.Slides.Remove(item);
                await _slideDbContext.SaveChangesAsync();
            }

            return item;
        }

        public async Task<IReadOnlyList<SlideEntity>> GetAllAsync()
        {
            return await _slideDbContext.Slides.ToListAsync();
        }
    }
}
