using System;
using System.Threading.Tasks;
using SyncView.Api.Entities;

namespace SyncView.Api.Repositories
{
    public interface IRoomRepository
    {
        Task<RoomEntity?> GetAsync(string id);
        Task<RoomEntity> CreateAsync(Guid slideUuid);
        Task<RoomEntity?> DeleteAsync(string id);
    }
}
