using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SyncView.Api.Entities;

namespace SyncView.Api.Repositories
{
    public interface ISlideRepository
    {
        Task<SlideEntity?> GetAsync(Guid uuid);
        Task<SlideEntity> AddAsync(SlideEntity item);
        Task<SlideEntity?> DeleteAsync(Guid uuid);
        Task<IReadOnlyList<SlideEntity>> GetAllAsync();
    }
}
