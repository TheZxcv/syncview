using System;
using System.Threading.Tasks;
using SyncView.Api.Entities;
using SyncView.Api.Utils;

namespace SyncView.Api.Repositories
{
    public class RoomRepository : IRoomRepository
    {
        private readonly SlideDbContext _slideDbContext;

        public RoomRepository(SlideDbContext slideDbContext)
        {
            _slideDbContext = Ensure.NotNull(slideDbContext, nameof(slideDbContext));
        }

        public async Task<RoomEntity?> GetAsync(string id)
        {
            return await _slideDbContext.Rooms.FindAsync(id);
        }

        public async Task<RoomEntity> CreateAsync(Guid slideUuid)
        {
            var item = new RoomEntity()
            {
                Id = RandomIdGenerator.Instance.NextId(),
                slideId = slideUuid,
            };

            // TODO: assumes the generated id is unique.
            await _slideDbContext.Rooms.AddAsync(item);
            await _slideDbContext.SaveChangesAsync();

            return item;
        }

        public async Task<RoomEntity?> DeleteAsync(string id)
        {
            var item = await GetAsync(id);
            if (item != null)
            {
                _slideDbContext.Rooms.Remove(item);
                await _slideDbContext.SaveChangesAsync();
            }

            return item;
        }
    }
}
