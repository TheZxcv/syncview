using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SyncView.Api.Entities;
using SyncView.Api.Hubs;
using SyncView.Api.Repositories;
using SyncView.Api.Services;
using SyncView.Api.Utils;

namespace SyncView.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<SlideDbContext>(
                opt => opt.UseSqlite("Data Source=slides.db")
            );

            services.AddDistributedMemoryCache();

            services.AddSingleton<AwsSettings>(Configuration
                    .GetSection("AWS")
                    .Get<AwsSettings>());
            services.AddSingleton<IWebStorageService, S3StorageService>();
            services.AddSingleton<IKeyValueStore, KeyValueStore>();

            services.AddScoped<ISlideRepository, SlideRepository>();
            services.AddScoped<ISlideService, SlideService>();
            services.AddScoped<IRoomRepository, RoomRepository>();
            services.AddScoped<IRoomService, RoomService>();

            services.AddRouting(opts => opts.LowercaseUrls = true);

            services.AddCors(options => options
                .AddDefaultPolicy(builder =>
                {
                    builder
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        // Same as AllowAnyOrigin
                        .SetIsOriginAllowed(_ => true)
                        .AllowCredentials();
                })
            );

            services.AddControllers()
                .AddJsonOptions(opts =>
                {
                    opts.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                });
            services.AddSignalR();

            services.AddAutoMapper(typeof(Startup).Assembly);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<RoomHub>("/echo/{roomId}");
            });

            // NOTE: must be last.
            var serviceScopeFactory = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>();
            using (var serviceScope = serviceScopeFactory.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetService<SlideDbContext>();
                dbContext.Database.EnsureCreated();
            }
        }
    }
}
