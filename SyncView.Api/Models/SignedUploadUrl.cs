using SyncView.Api.Utils;

namespace SyncView.Api.Models
{
    public class SignedUploadUrl
    {
        public SignedUploadUrl(string url)
        {
            Url = Ensure.NotNull(url, nameof(url));
        }

        public string Url { get; }
    }
}
