using System;

namespace SyncView.Api.Models
{
    public class SlideMetadata
    {
        public Guid Uuid { get; set; } = Guid.Empty;
        public string FileName { get; set; } = string.Empty;
        public string Description { get; set; } = string.Empty;
    }
}
