using System;

namespace SyncView.Api.Models
{
    public class CreateRoom
    {
        /// <summary>
        /// The slide UUID.
        /// </summary>
        public Guid Uuid { get; set; }
    }
}
