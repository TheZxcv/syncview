namespace SyncView.Api.Models
{
    public class Slide : SlideMetadata
    {
        public string? Url { get; set; }
    }
}
