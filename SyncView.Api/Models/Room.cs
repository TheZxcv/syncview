namespace SyncView.Api.Models
{
    public class Room
    {
        public string Id { get; set; } = string.Empty;
        public Slide Slide { get; set; } = null!;
    }
}
