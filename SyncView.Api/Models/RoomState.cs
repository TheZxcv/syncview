namespace SyncView.Api.Models
{
    public class RoomState
    {
        public RoomState() { Page = 1; }

        public RoomState(int page)
        {
            Page = page;
        }

        public int Page { get; set; }
    }
}
