namespace SyncView.Api.Models
{
    public class FileMetadata
    {
        public string? FileName { get; set; }
        public string? FileType { get; set; }
    }
}
