using AutoMapper;
using SyncView.Api.Entities;
using SyncView.Api.Models;

namespace SyncView.Api
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<SlideEntity, SlideMetadata>().ReverseMap();
            CreateMap<SlideEntity, Slide>()
                .ForMember(x => x.Url, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<RoomEntity, Room>()
                .ForMember(dst => dst.Slide,
                    opts => opts.MapFrom(
                        src => new Slide()
                        {
                            Uuid = src.slideId
                        }
                    ))
                .ReverseMap()
                .ForMember(dst => dst.slideId,
                    opts => opts.MapFrom(
                        src => src.Id
                    ));
        }
    }
}
