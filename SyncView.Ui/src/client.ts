const config = {
    serverApi: 'http://localhost:5000'
};

interface SlideCreation {
    fileName: string;
    description: string;
}

interface Slide extends SlideCreation {
    uuid: string;
    url: string;
}

interface Room {
    id: string;
    slide: Slide;
}

type ProgressFn = (_: number) => void;

class ApiClient {
    private endpoint: string;

    constructor(endpoint: string) {
        this.endpoint = endpoint;
    }

    uploadSlide(slide: SlideCreation, file: File, progressFn: ProgressFn | undefined): Promise<Slide> {
        return this.createSlide(slide)
            .then(response => {
                return this.upload(response.uuid, file, progressFn)
                    .then(_ => response);
            });
    }

    createSlide(slide: SlideCreation): Promise<Slide> {
        console.log('Create slide...');
        return fetch(`${this.endpoint}/slide`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(slide)
        }).then(response => response.json());
    }

    upload(uuid: string, file: File, progressFn: ProgressFn | undefined): Promise<any> {
        return this.fetchSignedRequest(uuid, file)
            .then(url => this.uploadFile(url, file, progressFn));
    }

    // Retrieve temporary signed request from the back-end.
    fetchSignedRequest(uuid: string, file: File): Promise<string> {
        return fetch(`${this.endpoint}/slide/${uuid}`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                fileName: file.name,
                fileType: file.type,
            })
        }).catch(function (response) {
            console.log('Could not get signed URL.');
            throw response;
        }).then(function (response) {
            return response.json();
        }).then(function (response: { url: string }) {
            return response.url;
        });
    }

    uploadFile(url: string, file: File, progressFn: ProgressFn | undefined): Promise<any> {
        const promise = new Promise<any>(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (e) {
                if (e.lengthComputable && progressFn) {
                    const uploaded = Math.round(100 * (e.loaded / e.total));
                    progressFn(uploaded);
                }
            });

            xhr.open('PUT', url);
            xhr.overrideMimeType(file.type);
            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(xhr.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            };;
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };;
            xhr.send(file);
        });

        return promise
            .then(function () {
                console.log('Upload done: ', url);
            }).catch(function (err) {
                console.log('Could not upload file: ', err);
                throw err;
            })
    }

    getSlide(uuid: string): Promise<Slide> {
        return fetch(`${this.endpoint}/slide/${uuid}`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => response.json());
    }

    getRoom(id: string): Promise<Room> {
        return fetch(`${this.endpoint}/room/${id}`, {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        }).then(response => response.json());
    }

    createRoom(uuid: string): Promise<Room> {
        return fetch(`${this.endpoint}/room`, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                uuid: uuid,
            }),
        }).then(response => response.json());
    }
}

interface PageChangeEvent {
    page: number;
}

class ViewerClient {
    onPageChange: (event: PageChangeEvent) => void = _ => { };
    private endpoint: string;
    private connection: signalR.HubConnection;

    constructor(endpoint: string, room: string) {
        this.endpoint = endpoint;
        this.connection = new signalR
            .HubConnectionBuilder()
            .withUrl(`${this.endpoint}/echo/${room}`)
            .build();

        this.connection.on('pageChange', (data: PageChangeEvent) => {
            console.log('received: ', data)
            if (typeof data.page !== 'undefined') {
                this.onPageChange(data)
            }
        });
    }

    start() {
        return this.connection.start();
    }

    sendPageChange(newPage: number): Promise<void> {
        console.log('send new page: ', newPage);
        const event: PageChangeEvent = {
            page: newPage,
        };
        return this.connection.invoke('ChangePage', event);
    }
}