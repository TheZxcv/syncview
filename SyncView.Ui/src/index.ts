const clientApi = new ApiClient(config.serverApi);

function beginUpload() {
    const progress = document.getElementById("upload-progress")!;
    progress.style.display = 'unset';
    const inputForm = document.getElementById("input-form")!;
    inputForm.style.display = 'none';

    setTimeout(function () {
        doUpload(new ProgressHandler())
    });
}

function doUpload(progress: ProgressHandler) {
    progress.init();

    const uploadInput = document.getElementById('upload') as HTMLInputElement;
    if (!uploadInput || !uploadInput.files || uploadInput.files.length === 0) {
        return;
    }

    const file = uploadInput.files[0];
    const description = 'blank';
    const data: SlideCreation = {
        fileName: file.name,
        description: description
    };

    clientApi.uploadSlide(data, file, p => progress.progress(p))
        .then(slide => clientApi.createRoom(slide.uuid))
        .then(room => { progress.finish(); return room; })
        .then(function (room) { showLink(room) })
        .catch(_ => progress.fail());

}

function showLink(room: Room) {
    const box = document.getElementById('room-link-box')! as HTMLDivElement;
    const roomLink = document.getElementById('room-link')! as HTMLInputElement;
    roomLink.value = `${document.location.origin}/viewer.html?room=${room.id}`;
    box.classList.remove('hidden');
}


class ProgressHandler {
    private circle: SVGCircleElement;
    private percentageText: SVGTextElement;
    private checkMark: SVGPathElement;
    private pathLength: number;

    constructor() {
        this.circle = <SVGCircleElement><any>document.getElementById("circle")!;
        this.percentageText = <SVGTextElement><any>document.getElementById("percentage")!;
        this.checkMark = <SVGPathElement><any>document.getElementById("checkmark")!;
        this.pathLength = Math.ceil(this.circle.getTotalLength());
    }

    init() {
        this.circle.style.strokeDasharray = this.pathLength.toString();
        this.circle.style.strokeDashoffset = this.pathLength.toString();

        this.checkMark.classList.remove("animate");
        this.percentageText.style.display = 'unset';

        this.circle.classList.add('circle-success');
        this.circle.classList.remove('circle-error');
    }

    progress(percentage: number) {
        this.percentageText.textContent = percentage.toString();

        let len = this.pathLength - percentage * (this.pathLength / 100);
        if (len < 0)
            len = 0;  // clamp to minimum 0

        this.circle.style.strokeDashoffset = len.toString();

        if (len <= 0) {
            this.finish();
        }
    }

    finish() {
        this.circle.style.strokeDashoffset = '0';
        this.checkMark.classList.add("animate");
        this.percentageText.style.display = 'none';
    }

    fail() {
        this.circle.style.strokeDashoffset = '0';
        this.circle.classList.remove('circle-success');
        this.circle.classList.add('circle-error');
        this.percentageText.style.display = 'none';
    }
}
