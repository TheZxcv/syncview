declare var pdfjsLib: any;

const urlParams = new URLSearchParams(window.location.search);
const room = urlParams.get('room')!;

interface AppState {
    pdf: any,
    currentPage: number,
    renderingTask: Promise<void> | null,
    pageRendering: boolean;
    pageNumPending: boolean;
    ready: boolean;
    room: string;
    client: MockViewerClient;
    apiClient: MockApiClient;
}

var appState: AppState = {
    pdf: null,
    currentPage: 1,
    renderingTask: null,
    pageRendering: false,
    pageNumPending: false,

    ready: false,
    room: room,
    client: new MockViewerClient(config.serverApi, room),
    apiClient: new MockApiClient(config.serverApi),
}

function resetCanvasSize(): void {
    const container = document.getElementById("container")!;
    const canvas = document.getElementById("viewer")! as HTMLCanvasElement;
    canvas.width = 2 * container.clientWidth;
    canvas.height = 2 * container.clientHeight;
}

function change(step: number) {
    const newPage = appState.currentPage + step
    if (!appState.ready || newPage <= 0 || newPage > appState.pdf.numPages) {
        return;
    }

    appState.client
        .sendPageChange(newPage)
        .then(response => console.log(response))
        .catch(error => console.log(error));
}

function initWebSocket() {
    appState.client.onPageChange = (msg) => {
        console.log('onPageChange', msg);
        appState.currentPage = +msg.page;
        renderPage();
    };

    appState.client.start()
        .then(function () {
            console.log('Ready!');
            appState.ready = true;
        }).catch(function (err) {
            return console.error(err.toString());
        });
}

function renderPage() {
    if (appState.pageRendering) {
        appState.pageNumPending = true;
        return;
    }

    appState.pageRendering = true;
    appState.pdf!.getPage(appState.currentPage).then((page) => {
        const canvas = document.getElementById("viewer")! as HTMLCanvasElement;
        const ctx = canvas.getContext('2d')!;

        const [_left, _top, width, height]: [number, number, number, number] = page.view;
        const ratio = width / height;
        canvas.width = canvas.height * ratio;

        const desiredHeight = canvas.height;
        const viewport = page.getViewport({ scale: 1, });
        const scale = desiredHeight / viewport.height;
        const scaledViewport = page.getViewport({ scale: scale, });

        appState.renderingTask = page.render({
            canvasContext: ctx,
            viewport: scaledViewport
        }).promise;

        appState.renderingTask!
            .then(function () {
                appState.pageRendering = false;
                if (appState.pageNumPending) {
                    renderPage();
                    appState.pageNumPending = false;
                }
            });
    });
}

function initViewer() {
    window.onresize = function () {
        resetCanvasSize();
        renderPage();
    };

    window.onkeyup = function (event: KeyboardEvent) {
        const e = event || window.event;

        switch (e.key) {
            case "Left": // IE/Edge
            case "ArrowLeft":
                change(-1);
                break;
            case "Right": // IE/Edge
            case "ArrowRight":
                change(+1);
                break;
        }
    };

    resetCanvasSize();
    renderPage();
}

appState
    .apiClient
    .getRoom(appState.room)
    .then(function (room) {
        return pdfjsLib.getDocument(room.slide.url).promise;
    })
    .then((pdf) => {
        appState.pdf = pdf;
        initViewer();
        // Enable websocket after the pdf has been loaded.
        initWebSocket();
    });