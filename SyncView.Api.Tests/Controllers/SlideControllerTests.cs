using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using ExpectedObjects;
using NUnit.Framework;
using SyncView.Api.Models;
using SyncView.Api.Tests.Utils;

namespace SyncView.Api.Tests.Controllers
{
    [Category("Integration")]
    [TestFixture]
    public class SlideControllerTests
    {
        private CustomWebApplicationFactory<Startup> _factory;
        private HttpClient _client;

        [OneTimeSetUp]
        public void SetUp()
        {
            _factory = new CustomWebApplicationFactory<Startup>();
            _client = _factory.CreateClient();
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            _factory.Dispose();
            _client.Dispose();
        }

        [Test]
        public async Task GetAllAsync_ReturnsEmptyList()
        {
            var response = await _client.GetAsync("slide");

            response.EnsureSuccessStatusCode();
            var responseObject = await JsonTestUtils.DeserializeFromContentAsync<List<SlideMetadata>>(response.Content);

            Assert.AreEqual(new List<SlideMetadata>(), responseObject);
        }

        [Test]
        public async Task PostSlideAsync_ReturnsCreated()
        {
            var slide = new Slide
            {
                FileName = "slide.pdf",
                Description = "description",
            };
            var expected = slide
                .ToExpectedObject(config => config.Ignore(s => s.Uuid));

            var response = await _client.PostAsync("slide/", JsonTestUtils.SerializeToContent(slide));

            response.EnsureSuccessStatusCode();
            Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);

            var responseObject = await JsonTestUtils.DeserializeFromContentAsync<Slide>(response.Content);
            Assert.NotNull(responseObject);
            Assert.AreNotEqual(Guid.Empty, responseObject.Uuid);
            expected.ShouldEqual(responseObject);
        }
    }
}
