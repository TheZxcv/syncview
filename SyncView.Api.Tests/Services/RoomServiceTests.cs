using System;
using System.Threading.Tasks;
using AutoMapper;
using ExpectedObjects;
using FakeItEasy;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using SyncView.Api.Entities;
using SyncView.Api.Models;
using SyncView.Api.Repositories;
using SyncView.Api.Services;

namespace SyncView.Api.Tests.Services
{
    [Category("Unit")]
    [TestFixture]
    public class RoomServiceTests
    {
        private readonly IMapper _mapper;
        private IRoomRepository _fakeRoomRepository;
        private ISlideService _fakeSlideService;
        private IKeyValueStore _fakeKVStore;
        private IRoomService _roomService;

        public RoomServiceTests()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>());
            config.AssertConfigurationIsValid();
            _mapper = config.CreateMapper();
        }

        [SetUp]
        public void SetUp()
        {
            _fakeRoomRepository = A.Fake<IRoomRepository>();
            _fakeSlideService = A.Fake<ISlideService>();
            _fakeKVStore = A.Fake<IKeyValueStore>();
            _roomService = new RoomService(NullLogger<IRoomService>.Instance,
                _fakeRoomRepository,
                _fakeSlideService,
                _fakeKVStore,
                _mapper);
        }

        [Test]
        public async Task GetAsync_ReturnsRoom()
        {
            var slide = new Slide()
            {
                Uuid = Guid.ParseExact("33333333-3333-3333-3333-333333333333", "D"),
                Url = "http://example.com",
            };
            var room = new RoomEntity()
            {
                Id = "randomId",
                slideId = slide.Uuid,
            };

            A.CallTo(() => _fakeRoomRepository.GetAsync(room.Id))
                .Returns(room);
            A.CallTo(() => _fakeSlideService.GetAsync(slide.Uuid))
                .Returns(slide);

            var expected = new Room()
            {
                Id = room.Id,
                Slide = slide,
            }.ToExpectedObject();

            var actual = await _roomService.GetAsync(room.Id);

            expected
                .ShouldEqual(actual);
        }

        [Test]
        public async Task GetAsync_ReturnsNullOnNotFound()
        {
            var nonexistentId = "nonexistentId";
            A.CallTo(() => _fakeRoomRepository.GetAsync(nonexistentId))
                .Returns(null as RoomEntity);

            var actual = await _roomService.GetAsync(nonexistentId);

            Assert.Null(actual);
        }
    }
}
