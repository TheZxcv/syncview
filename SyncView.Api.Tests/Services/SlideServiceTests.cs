using System;
using System.Threading.Tasks;
using AutoMapper;
using ExpectedObjects;
using FakeItEasy;
using Microsoft.Extensions.Logging.Abstractions;
using NUnit.Framework;
using SyncView.Api.Entities;
using SyncView.Api.Models;
using SyncView.Api.Repositories;
using SyncView.Api.Services;

namespace SyncView.Api.Tests.Services
{
    [Category("Unit")]
    [TestFixture]
    public class SlideServiceTests
    {
        private readonly IMapper _mapper;
        private ISlideRepository _fakeSlideRepository;
        private IWebStorageService _fakeStorageService;
        private ISlideService _slideService;

        public SlideServiceTests()
        {
            var config = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>());
            config.AssertConfigurationIsValid();
            _mapper = config.CreateMapper();
        }

        [SetUp]
        public void SetUp()
        {
            _fakeSlideRepository = A.Fake<ISlideRepository>();
            _fakeStorageService = A.Fake<IWebStorageService>();
            _slideService = new SlideService(NullLogger<ISlideService>.Instance,
                _fakeSlideRepository,
                _fakeStorageService,
                _mapper);
        }

        [Test]
        public async Task GetAsync_ReturnsFoundSlideWithUrl()
        {
            var uuid = Guid.ParseExact("33333333-3333-3333-3333-333333333333", "D");
            var expected = new Slide()
            {
                Uuid = uuid,
                FileName = "sample.pdf",
                Description = "This is a sample PDF.",
                Url = "https://example.org/sample.pdf",
            };

            A.CallTo(() => _fakeSlideRepository.GetAsync(uuid))
                .Returns(new SlideEntity()
                {
                    Uuid = expected.Uuid,
                    FileName = expected.FileName,
                    Description = expected.Description,
                });

            A.CallTo(() => _fakeStorageService.GetUrlAsync(uuid))
                .Returns(expected.Url);

            var actual = await _slideService.GetAsync(uuid);

            expected
                .ToExpectedObject()
                .ShouldEqual(actual);
        }

        [Test]
        public async Task GetAsync_ReturnsNullOnNotFound()
        {
            var uuid = Guid.ParseExact("33333333-3333-3333-3333-333333333333", "D");

            A.CallTo(() => _fakeSlideRepository.GetAsync(uuid))
                .Returns(null as SlideEntity);


            var actual = await _slideService.GetAsync(uuid);

            A.CallTo(() => _fakeStorageService.GetUrlAsync(A<Guid>.Ignored))
                .MustNotHaveHappened();
            Assert.Null(actual);
        }
    }
}
