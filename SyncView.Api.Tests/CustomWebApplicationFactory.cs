using System;
using System.Linq;
using FakeItEasy;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using SyncView.Api.Entities;
using SyncView.Api.Services;

namespace SyncView.Api.Tests
{
    public class CustomWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {
        public IWebStorageService FakeWebStorageService { get; } = A.Fake<IWebStorageService>();

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services
                    .Replace(ServiceDescriptor.Singleton(FakeWebStorageService));

                var descriptor = services.SingleOrDefault(
                    d => d.ServiceType == typeof(DbContextOptions<SlideDbContext>)
                );

                services.Remove(descriptor);

                services.AddDbContext<SlideDbContext>(options =>
                {
                    // Ensures each test gets its own database.
                    options.UseInMemoryDatabase(Guid.NewGuid().ToString());
                });

                var serviceProvider = services.BuildServiceProvider();

                using (var scope = serviceProvider.CreateScope())
                {
                    var scopedServices = scope.ServiceProvider;
                    var db = scopedServices.GetRequiredService<SlideDbContext>();
                    var logger = scopedServices
                        .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                    db.Database.EnsureCreated();
                }
            });
        }
    }
}
