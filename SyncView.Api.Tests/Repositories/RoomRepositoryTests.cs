using System;
using System.Linq;
using System.Threading.Tasks;
using ExpectedObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NUnit.Framework;
using SyncView.Api.Entities;
using SyncView.Api.Repositories;

namespace SyncView.Api.Tests.Repositories
{
    [Category("Unit")]
    [TestFixture]
    public class RoomRepositoryTests
    {
        private SlideDbContext _slideContext;
        private RoomRepository _roomRepository;

        [SetUp]
        public void SetUp()
        {
            var dbOptions = new DbContextOptionsBuilder<SlideDbContext>()
                .UseInMemoryDatabase("TestDb",
                    /* guarantees each test gets a clean db */
                    new InMemoryDatabaseRoot())
                .Options;
            _slideContext = new SlideDbContext(dbOptions);
            _roomRepository = new RoomRepository(_slideContext);
        }

        [TearDown]
        public void TearDown()
        {
            _slideContext.Dispose();
        }

        [Test]
        public async Task GetAsync_ReturnsTheCorrectOne()
        {
            var expected = new RoomEntity()
            {
                Id = "RandomId",
                slideId = Guid.Empty,
            };
            await _slideContext.Rooms.AddAsync(expected);
            await _slideContext.SaveChangesAsync();

            var found = await _roomRepository.GetAsync(expected.Id);

            Assert.NotNull(found);
            expected.ToExpectedObject().ShouldEqual(found);
        }

        [Test]
        public async Task AddAsync_Succeeds()
        {
            var slide = new SlideEntity()
            {
                Uuid = Guid.ParseExact("12345678-9abc-def0-1234-56789abcdef0", "D"),
                FileName = "sample.pdf",
                Description = "This is a sample description",
            };
            await _slideContext.Slides.AddAsync(slide);
            await _slideContext.SaveChangesAsync();

            var room = await _roomRepository.CreateAsync(slide.Uuid);

            Assert.NotNull(room);
            Assert.AreEqual(slide.Uuid, room.slideId);
            Assert.AreEqual(1, _slideContext.Rooms.Count());
        }

        [Test]
        public async Task DeleteAsync_ShouldDeleteItem()
        {
            var expected = new RoomEntity()
            {
                Id = "RandomId",
                slideId = Guid.Empty,
            };
            await _slideContext.Rooms.AddAsync(expected);
            await _slideContext.SaveChangesAsync();

            var deleted = await _roomRepository.DeleteAsync(expected.Id);

            Assert.NotNull(deleted);
            expected.ToExpectedObject().ShouldEqual(deleted);
            Assert.IsEmpty(_slideContext.Rooms);
        }
    }
}
