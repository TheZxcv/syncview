using System;
using System.Threading.Tasks;
using ExpectedObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using NUnit.Framework;
using SyncView.Api.Entities;
using SyncView.Api.Repositories;

namespace SyncView.Api.Tests.Repositories
{
    [Category("Unit")]
    [TestFixture]
    public class SlideRepositoryTests
    {
        private SlideDbContext _slideContext;
        private SlideRepository _slideRepository;

        [SetUp]
        public void SetUp()
        {
            var dbOptions = new DbContextOptionsBuilder<SlideDbContext>()
                .UseInMemoryDatabase("TestDb",
                    /* guarantees each test gets a clean db */
                    new InMemoryDatabaseRoot())
                .Options;
            _slideContext = new SlideDbContext(dbOptions);
            _slideRepository = new SlideRepository(_slideContext);
        }

        [TearDown]
        public void TearDown()
        {
            _slideContext.Dispose();
        }

        [Test]
        public async Task AddAsync_Succeeds()
        {
            var toAdd = new SlideEntity()
            {
                FileName = "sample.pdf",
                Description = "Sample description",
            };

            var result = await _slideRepository.AddAsync(toAdd);

            toAdd
                .ToExpectedObject(config => config.Ignore(c => c.Uuid))
                .ShouldEqual(result);

            var fromDb = await _slideContext.Slides.FindAsync(result.Uuid);
            Assert.NotNull(fromDb);
            fromDb
                .ToExpectedObject()
                .ShouldEqual(result);
        }

        [Test]
        public async Task DeleteAsync_ShouldDeleteItem()
        {
            var expected = new SlideEntity()
            {
                Uuid = Guid.ParseExact("12345678-9abc-def0-1234-56789abcdef0", "D"),
                FileName = "sample.pdf",
                Description = "This is a sample description",
            };
            await _slideContext.Slides.AddAsync(expected);
            await _slideContext.SaveChangesAsync();

            var deleted = await _slideRepository.DeleteAsync(expected.Uuid);

            Assert.NotNull(deleted);
            expected.ToExpectedObject().ShouldEqual(deleted);
            Assert.IsEmpty(_slideContext.Slides);
        }

        [Test]
        public async Task DeleteAsync_ShouldDoNothing()
        {
            await _slideContext.Slides.AddAsync(new SlideEntity()
            {
                Uuid = Guid.ParseExact("11111111-1111-1111-1111-111111111111", "D"),
                FileName = "sample.pdf",
                Description = "This is a sample description",
            });
            await _slideContext.SaveChangesAsync();

            var uuid = Guid.ParseExact("22222222-2222-2222-2222-222222222222", "D");
            var deleted = await _slideRepository.DeleteAsync(uuid);

            Assert.Null(deleted);
            Assert.IsNotEmpty(_slideContext.Slides);
        }
    }
}
