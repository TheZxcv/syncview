using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SyncView.Api.Tests.Utils
{
    public static class JsonTestUtils
    {
        public static HttpContent SerializeToContent(object item)
        {
            return new StringContent(
                JsonSerializer.Serialize(item),
                Encoding.UTF8,
                "application/json"
            );
        }

        public static async Task<T> DeserializeFromContentAsync<T>(HttpContent content) where T : class
        {
            Assert.NotNull(content);

            return JsonSerializer.Deserialize<T>(
                await content.ReadAsStringAsync(),
                new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true
                });
        }
    }
}
